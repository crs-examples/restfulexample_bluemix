package com.mkyong.rest;

import java.io.IOException;
import java.io.InputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.crs4bluemix.CRS4BluemixFile;
import com.crs4bluemix.CRS4BluemixFileOutputStream;
import com.crs4bluemix.CRS4BluemixFilePayload;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/file")
public class UploadFileService {

	@POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
			@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail) {

		String uploadedFileLocation = fileDetail.getFileName();
		// save it
		writeToFile(uploadedInputStream, uploadedFileLocation);
		String output = "File uploaded to : " + uploadedFileLocation;
		return Response.status(200).entity(output).build();

	}

	private void writeToFile(InputStream uploadedInputStream,
			String uploadedFileLocation) {
		try {
			CRS4BluemixFilePayload payload = new CRS4BluemixFilePayload(uploadedInputStream);
			CRS4BluemixFileOutputStream out = new CRS4BluemixFileOutputStream(new CRS4BluemixFile(
					uploadedFileLocation));
			out.write(payload);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}