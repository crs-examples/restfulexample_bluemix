package com.crs4bluemix;

import org.openstack4j.api.OSClient.OSClientV3;
import org.openstack4j.api.storage.ObjectStorageService;
import org.openstack4j.model.common.Identifier;
import org.openstack4j.openstack.OSFactory;

public class CRSUtils {
	private static final String USERNAME = "admin_7e7faf8e9e3ff2255437106458de353ee1c6e134";
	private static final String PASSWORD = "r8,/V1Z?55N4KE-l";
	private static final String DOMAIN_ID = "159da50538c748a38b02d4b671707c2e";
	private static final String PROJECT_ID = "5de7f3e742284180b727ede58ea81da5";
	
	static ObjectStorageService authenticateAndGetObjectStorageService() {
		String OBJECT_STORAGE_AUTH_URL = "https://identity.open.softlayer.com/v3";

		Identifier domainIdentifier = Identifier.byId(DOMAIN_ID);

		OSClientV3 os = OSFactory.builderV3()
				.endpoint(OBJECT_STORAGE_AUTH_URL)
				.credentials(USERNAME,PASSWORD, domainIdentifier)
				.scopeToProject(Identifier.byId(PROJECT_ID))
				.authenticate();

		ObjectStorageService objectStorage = os.objectStorage();

		return objectStorage;
	}
}
