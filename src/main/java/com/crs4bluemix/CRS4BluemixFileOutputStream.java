package com.crs4bluemix;

import java.io.IOException;

import org.openstack4j.api.storage.ObjectStorageService;

public class CRS4BluemixFileOutputStream extends CRSFileOutputStream {
	private CRS4BluemixFile bluemixFile;
	private CRS4BluemixFilePayload bluemixPayload;
	
	public CRS4BluemixFileOutputStream(CRS4BluemixFile bluemixFile) {
		this.bluemixFile = bluemixFile;
	}
	
	@Override
	public void close() throws IOException {
		if (this.bluemixPayload != null) {
			bluemixPayload.close();
		}
	}

	@Override
	public void write(CRS4BluemixFilePayload bluemixPayload) {
		this.bluemixPayload = bluemixPayload;
	}
	
	@Override
	public void flush() {
		if (this.bluemixFile != null && this.bluemixPayload != null) {
			ObjectStorageService objectStorage = CRSUtils.authenticateAndGetObjectStorageService();
			
			objectStorage.objects().put(
					bluemixFile.getContainerName(), 
					bluemixFile.getPath(), 
					bluemixPayload
			);
		}
	}
}
