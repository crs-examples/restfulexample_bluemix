package com.crs4bluemix;

import java.io.IOException;

/**
* CRSFileOutputStream.java - Javadoc under construction.
* @author Erick Barros
* @version 1.0
*/
public abstract class CRSFileOutputStream {
	public abstract void close() throws IOException;
	public abstract void write(CRS4BluemixFilePayload bluemixPayload);
	public abstract void flush();
}
