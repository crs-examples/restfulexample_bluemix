package com.crs4bluemix;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.openstack4j.api.storage.ObjectStorageService;
import org.openstack4j.model.common.ActionResponse;
import org.openstack4j.model.storage.object.SwiftObject;

import com.crs4bluemix.CRSUtils;

/**
* CRS4BluemixFile.java - Javadoc under construction.
* @author Erick Barros
* @version 1.0
*/
public class CRS4BluemixFile extends CRSFile {
	private String containerName = "crs-container";
	private String pathName;
	
	public CRS4BluemixFile(String pathName) {
		this.pathName = pathName;
	}
	
	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	@Override
	public String getPath() {
		return this.pathName;
	}

	@Override
	public String getAbsolutePath() {
		return this.getPath();
	}
	
	@Override
	public boolean exists() {
		ObjectStorageService objectStorage = CRSUtils.authenticateAndGetObjectStorageService();
		SwiftObject object = objectStorage.objects().get(getContainerName(), getName());
		if (object == null) {
			return false;
		}
		return true;
	}
	
	@Override
	public CRSFile[] listFiles() {
		ObjectStorageService objectStorage = CRSUtils.authenticateAndGetObjectStorageService();
		List<? extends SwiftObject> cloudFileList = objectStorage.objects().list(getContainerName());
		List<CRS4BluemixFile> fileList = new ArrayList<CRS4BluemixFile>();
		
		if (!this.isDirectory()) {
			return null;
		} 
		String pathCloudFile = getPath();
		for (SwiftObject file : cloudFileList) {
			String fileName = file.getName();
			if (!fileName.equals(pathCloudFile) && fileName.startsWith(pathCloudFile)) {
				fileList.add(new CRS4BluemixFile(fileName));
			}
		}
		return fileList.toArray(new CRS4BluemixFile[fileList.size()]);
	}

	@Override
	public CRSFile[] listFiles(CRSFilenameFilter filter) {
		return null;
	}

	@Override
	public String getName() {
		// TODO
		String[] arrayPath = new String[this.getPath().split("-").length];
		arrayPath = this.getPath().split("-");
		
		if (arrayPath.length > 1) {			
			return arrayPath[arrayPath.length - 1];
		} else {
			return this.getPath(); 
		}
	}

	@Override
	public boolean delete() {
		ObjectStorageService objectStorage = CRSUtils.authenticateAndGetObjectStorageService();
		ActionResponse deleteResponse = objectStorage.objects().delete(getContainerName(),this.getPath());
		
		if(deleteResponse.isSuccess()){
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean mkdirs() {
	    InputStream targetStream = null;
	    String directoryName = null;
	    
	    if (this.isDirectory()) {
	    	directoryName = this.getPath();
	    } else {
	    	directoryName = this.getPath().replaceAll(this.getName(), "");
	    }

	    try {
	    	File dirFile = new File(directoryName);
	    	dirFile.createNewFile();
			targetStream = new FileInputStream(dirFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	    CRS4BluemixFile cloudDir = new CRS4BluemixFile(directoryName);
	    CRS4BluemixFileOutputStream fileOutputStream = new CRS4BluemixFileOutputStream(cloudDir);
	    fileOutputStream.write(new CRS4BluemixFilePayload(targetStream));	    	
	    fileOutputStream.flush();
	    
	    return cloudDir.exists() ? true : false;
	}
	
	@Override
	public boolean isDirectory() {
		String[] arrayPath = new String[this.getPath().split("-").length];
		arrayPath = this.getPath().split("-");
		return (!arrayPath[arrayPath.length - 1].contains(".")) ? true : false;
	}
}