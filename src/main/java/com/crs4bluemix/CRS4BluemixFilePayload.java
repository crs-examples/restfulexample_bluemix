package com.crs4bluemix;

import java.io.IOException;
import java.io.InputStream;

import org.openstack4j.model.common.Payload;

public class CRS4BluemixFilePayload implements Payload<InputStream> {
	private InputStream stream;

	public CRS4BluemixFilePayload(InputStream stream) {
		this.stream = stream;
	}

	@Override
	public void close() throws IOException {
		stream.close();
	}

	@Override
	public InputStream open() {
		return stream;
	}

	@Override
	public void closeQuietly() {
		try {
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public InputStream getRaw() {
		return stream;
	}
}
